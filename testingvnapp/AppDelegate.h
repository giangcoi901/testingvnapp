//
//  AppDelegate.h
//  testingvnapp
//
//  Created by doanthegiang on 5/26/17.
//  Copyright © 2017 giang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

